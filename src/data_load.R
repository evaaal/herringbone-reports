# data_load
wlog("Querying Digital Analytics", head = T)
wlog("===========================")
wlog("Accessing IBM Digital Analytics", head = T)


fmat_ <- 'json'

periodicity <- 'daily'

N_DAYS<-14

wlog("Gathering the data from digital analytics review of laydowns views and sessions",level=3)
#Gathering the information for the last 14 days
laydown_performance <-
  tibble(date = Sys.Date() - 1:N_DAYS) %>%
  dplyr::mutate(page_view = map(.x =
                                  date,  ~ {
                                    #Create a json query for the daily reports
                                    #from Digital Analytics
                                    blob <-
                                      jsonlite::fromJSON(da_report(
                                        fmat = fmat_,
                                        view = 'adhoc',
                                        report = names(reports)[[4]],
                                        period = 'daily',
                                        date = .x
                                      ))
                                    
                                    #From the blob, go to the Data - Rows portion of the object
                                    #and transpose the resulting array into a dataframe format
                                    df <-
                                      as.data.frame(t(sapply(blob[['Data']][['Rows']], function(x)
                                        t(as.data.frame(
                                          x
                                        ))[-1,])), stringsAsFactors = F) %>%
                                      #Upon converting the format to a data.frame, rename the columns according to
                                      #how they appear in DA
                                      setNames(., c(blob[['Data']][['TotalRow']][['columnId']]))
                                    return(df)
                                  }))

wlog("Gathering the data from digital analytics review of laydowns and product purchases",level=3)
#Gathering the information for the last 14 days
hb_sales <-
  tibble(date = Sys.Date() - 1:N_DAYS) %>%
  dplyr::mutate(sales = map(.x =
                                  date,  ~ {
                                    #Create a json query for the daily reports
                                    #from Digital Analytics
                                    blob <-
                                      jsonlite::fromJSON(da_report(
                                        fmat = fmat_,
                                        view = 'adhoc',
                                        report = names(reports)[[3]],
                                        period = 'daily',
                                        date = .x
                                      ))
                                    
                                    #From the blob, go to the Data - Rows portion of the object
                                    #and transpose the resulting array into a dataframe format
                                    df <-
                                      as.data.frame(t(sapply(blob[['Data']][['Rows']], function(x)
                                        t(as.data.frame(
                                          x
                                        ))[-1,])), stringsAsFactors = F) %>%
                                      #Upon converting the format to a data.frame, rename the columns according to
                                      #how they appear in DA
                                      setNames(., c(blob[['Data']][['TotalRow']][['columnId']]))
                                    return(df)
                                  }))



hb_sales %>%
  #For any given date that did not create data, exclude them from the union
  dplyr::filter(map_lgl(.x = sales,  ~ ncol(.) > 0)) %>%
  #Expand the data existing on any given data
  tidyr::unnest() %>%
  #Translate the corresponding actions for something a little more digestable
  # to clients
  dplyr::mutate(
    MARKETING_CATEGORY = if_else(
      MARKETING_CATEGORY == "share",
      "Product Shared",
      "Laydown Shared"
    ),
    #For the AD IDs present in DA, force them to be upper case to ease the matching
    MARKETING_PLACEMENT = toupper(MARKETING_PLACEMENT)
  ) %>%
  #Based on preliminary test, remove the irrelevant users who cannot be found in Slogix
  dplyr::filter(!grepl(paste0(IGNORE_USERS, collapse = "|"), MARKETING_PLACEMENT)) %>%
  group_by(date, MARKETING_PLACEMENT, MARKETING_CATEGORY,MARKETING_ITEM) %>% dplyr::summarise(
    item_value = sum(as.numeric(gsub("[[:punct:]]|CA|US","",ITEM_SALES))/100,na.rm=T)
  )%>%ungroup(.) ->  hb_df

wlog("Expand the laydowns by advisor",level=3)

laydown_performance %>%
  #For any given date that did not create data, exclude them from the union
  dplyr::filter(map_lgl(.x = page_view,  ~ ncol(.) > 0)) %>%
  #Expand the data existing on any given data
  tidyr::unnest() %>%
  #Translate the corresponding actions for something a little more digestable
  # to clients
  dplyr::mutate(
    MARKETING_CATEGORY = if_else(
      MARKETING_CATEGORY == "share",
      "Product Shared",
      "Laydown Shared"
    ),
    #For the AD IDs present in DA, force them to be upper case to ease the matching
    MARKETING_PLACEMENT = toupper(MARKETING_PLACEMENT)
  ) %>%
  #Based on preliminary test, remove the irrelevant users who cannot be found in Slogix
  dplyr::filter(!grepl(paste0(IGNORE_USERS, collapse = "|"), MARKETING_PLACEMENT)) %>%
  .[!duplicated(.),] %>%
  dplyr::select(-2) %>%
  group_by(date, MARKETING_PLACEMENT, MARKETING_CATEGORY) %>% dplyr::summarise(
    items = length(unique(MARKETING_ITEM)),
    sessions = sum(as.numeric(SESSIONS)),
    page_views = sum(as.numeric(PAGE_VIEWS)),
    product_views = sum(as.numeric(PRODUCT_VIEWS)),
    items_added = sum(as.numeric(ITEMS_ADDED))
  ) %>% ungroup(.) ->  lp_df

laydown_performance %>%
  #For any given date that did not create data, exclude them from the union
  dplyr::filter(map_lgl(.x = page_view,  ~ ncol(.) > 0)) %>%
  #Expand the data existing on any given data
  tidyr::unnest()%>%dplyr::mutate(
    MARKETING_CATEGORY = if_else(
      MARKETING_CATEGORY == "share",
      "Product Shared",
      "Laydown Shared"
    ),
    #For the AD IDs present in DA, force them to be upper case to ease the matching
    MARKETING_PLACEMENT = toupper(MARKETING_PLACEMENT)
  )->lp_raw

#Unique DA Users to send messages out to
da_users<-unique(lp_df$MARKETING_PLACEMENT)

#Connect to Sales Logix
slxConnect()
wlog("Get the users from Saleslogix",level=2)

#Gather the data for associates to begin matching with Slogix DBs
getDataFromServer(
  beans,
  sqlFile = "",
  statement = paste0("select ui.title, st.storename ,up.USERTEXT1,ui.userid, ui.firstname, ui.username, ui.EMAIL, us.usercode
from sysdba.USERINFO ui
inner join SYSDBA.USERSECURITY us
on us.USERID = ui.USERID
inner join SYSDBA.userprofile up
on us.USERID = up.USERID
inner join hri.STORE_REGION_LEVELS st
on st.STORECODE= up.USERTEXT1
where firstname is not null
  and firstname not like '%Copy%'
  and firstname not like '%Admin%'
  and firstname not like '%SLX%'
  and firstname not like '%STORE%'
  and lower(username) not like '%assoc%'
--and title like '%Associate%'
and username not like '%Copy%'
and EMAIL is not null
and EMAIL like '%@%'
and ENABLED='T'
and length(us.usercode)<8
and up.usertext1 in (",paste0("'",STORES,"'",collapse=","),")
order by lastname"),
  name = "users"
)
wlog("Getting the store managers data",level=2)

getDataFromServer(
  beans,
  sqlFile = "",
  statement = paste0("select r.rolename, ui.firstname, ui.lastname, ui.email, st.storename, us.usercode, ui.MODIFYDATE,up.usertext1 STORECODE
  , md.firstname || ' ' || md.lastname ManagingDirector, md.email ManagingDirectorEmail
from sysdba.role r
  inner join sysdba.userrole ur on ur.roleid = r.roleid
  inner join sysdba.usersecurity us on ur.userid = us.userid
  inner join sysdba.userinfo ui on ui.userid = us.userid
  inner join sysdba.userprofile up on up.userid = us.userid
  inner join hri.store_region_levels st on st.storecode = up.usertext1
  left join sysdba.userinfo md on md.userid = us.managerid
where us.enabled = 'T'
  and lower(ui.firstname) not like 'jesta%'
  and lower(ui.firstname) not like 'racq%'
  and lower(ui.firstname) not like 'slx%'
  and lower(ui.firstname) not like 'shippers%'
  and lower(ui.firstname) not like 'copy%'
  and lower(ui.firstname) not like 'admin%'
  and lower(ui.firstname) not like 'marketing%'
  and lower(ui.firstname) not like 'virtual%'
  and lower(ui.LASTNAME) not like 'generic%'
  and lower(up.usertext1) not like 'co'
  and length(us.usercode) < 7
  and r.rolename ='Store Mgr'
and up.usertext1 in (",paste0("'",STORES,"'",collapse=","),")
order by up.usertext1"),
  name = "mgrs"
)


wlog("Getting the activity of sharing with clients where Herringbone is the source of the data",level=2)


#Gather the data for associates to begin matching with Slogix DBs
getDataFromServer(
  beans,
  sqlFile = "",
  statement =
    "select historyid,
       type,
       se.contactid,
       se.CUSTOMERIDTEMP,
       contactname,
       h.createdate,
       userid,
       username,
       userdef3
from sysdba.HISTORY h
         inner join SALESENTITY se on h.CONTACTID = se.CONTACTID
where lower(h.CREATESOURCE) = 'herringbone'
  and trunc(h.completeddate) > sysdate - 14",
  name = "share_history"
)

wlog("Gathering the transactions associated with clients who have been engaged via Herringbone",level=2)

#Gather the data for associates to begin matching with Slogix DBs
getDataFromServer(
  beans,
  sqlFile = "",
  statement =
    "with clients as(select  distinct contactid, type, contactname,createdate,userid,username,userdef3
from sysdba.HISTORY h
where lower(h.CREATESOURCE) = 'herringbone'
and trunc(completeddate) > sysdate-14
order by CREATEDATE desc)
select tr.BUSINESSDATE,
       tr.transtart,
       se.CUSTOMERID,
       se.CONTACTID,
       c.lastname || ', '||c.firstname contactname,
       c.accountmanagerid,
       lpad(ti.fkemployeeno,7,'0') fkemployeeno,
       tr.PKTRANSACTIONID,
       t.FKSTORENO,
       ti.PKTRANITEMID,
       sy.div_cd                                   dept,
       sy.dept_cd                                  subd,
       trim(replace(sy.classdsc, sy.deptdesc, '')) brand,
       sy.description                              subcl,
       sy.style_id,
       sum(ti.qty) qty,
       sum(ti.SELLINGPRICE * ti.QTY / 100)         val,
       SUM(CASE
               WHEN MOD(ti.sellingprice, 10) = 9
                   OR Nvl(tid.amount, 0) <> 0 THEN 1
               ELSE 0
           END) AS                                 mkdn,
       case when sum(ti.SELLINGPRICE * ti.QTY / 100)  > 0 then 1 else 0 end as purchase
from sysdba.contact c
         inner join clients cl
                    on cl.contactid = c.contactid
         inner join sysdba.contact_retail cr
                    on cr.contactid = c.contactid
         inner join sysdba.contact_profile cp
                    on c.contactid = cp.contactid
         inner join sysdba.salesentity se
                    on se.contactid = c.contactid
         inner join vstore.customer cu
                    on cu.identificationno = se.customerid
         inner join vstore.transaction tr
                    on tr.fkcustomerno = cu.pkcustomerno
         inner join vstore.tranitem ti
                    on ti.fktransactionid = tr.pktransactionid
                       left outer join (select FKTRANITEMID, sum(AMOUNT) amount
                                        from vstore.TRANITEMDISCOUNT
                                        group by FKTRANITEMID) tid
                                       on tid.fktranitemid = ti.PKTRANITEMID
         inner join vstore.terminal t
                    on tr.fkterminalid = t.pkterminalid
--         inner join vstore.store s
--                    on s.FKSRCSTORESERVERID= t.FKSRCSTORESERVERID
         inner join vstore.sku k
                    on k.pksku = ti.fksku
         inner join custom.style sy
                    on sy.style_id = k.fkstyleno
where trunc(tr.businessdate) > sysdate - 14
  and tr.isvoided = 'f'
  and tr.fktrantype = 10
  and ti.isvoided = 'f'
  and nvl(c.status, 'New') != 'Dormant'
  and nvl(c.type, '01') = '01'
  and c.seccodeid in ('F6UJ9A000002')
/*  and (((c.workphone || c.email || c.homephone || c.mobile) is not null
    AND
        NVL(C.DONOTPHONE, 'F') = 'F')
    OR
       (C.EMAIL IS NOT NULL AND NVL(CR.COM_ASSOCIATE, 'F') = 'T'))*/
group by tr.transtart, tr.BUSINESSDATE, se.CUSTOMERID,se.CONTACTID,c.lastname || ', '||c.firstname,c.accountmanagerid,ti.fkemployeeno, tr.PKTRANSACTIONID,t.FKSTORENO, ti.PKTRANITEMID, sy.div_cd, sy.dept_cd,
         trim(replace(sy.classdsc, sy.deptdesc, '')), sy.description, sy.style_id
order by businessdate desc;",
  name = "hb_trans_history"
)