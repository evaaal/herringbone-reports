wlog("Send E-mails to Managers", head = T)
wlog("=========================")

#Production-ready mass e-mail
if (RUN_ENV == "PROD")
{
  sapply(STORES, function(x) {
    #Identify the managers for a given store
    RECIPS <- mgr_recips[[x]]
    #Gather the report created for the corresponding
    email_body <- sendmailR::mime_part(documents_send[[x]])
    email_body[["headers"]][["Content-Type"]] <- "text/html"
    
    if(nrow(adv_int[[x]])>0)c_send_report <- tryCatch(
      expr = {
        wlog("Distributing report...", head = T)
        wlog(level = 3)
        mailstatus <- sendEmail(recipientList = USER,
        # mailstatus <- sendEmail(recipientList = RECIPS
                                # ,
                                subject = 'TEST: Herringbone Performance for Management',
                                bcc = USER,
                                body = email_body)
        wlog(paste0("mail send status code:     ", mailstatus$code), level = 3)
        wlog(paste0("mail send status message:  ", mailstatus$mdg),
             level = 3)
        mailstatus$code %in% c("220", "221")
      },
      error = function(err) {
        wlog("An error occurred sending the email out")
        wlog(err, level = 2)
        return(FALSE)
      }
    )
  })
  
}
